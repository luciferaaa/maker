package ldh.maker.freemaker;

public class MainControllerMaker extends FreeMarkerMaker<MainControllerMaker> {

	protected String controllerPackage;

	public MainControllerMaker() {

	}

	public MainControllerMaker controllerPackage(String controllerPackage) {
		this.controllerPackage = controllerPackage;
		return this;
	}
	
	public void data() {
		fileName = "MainController.java";
		data.put("fileName", fileName);
		data.put("controllerPackage", controllerPackage);
		check();
	}

	public void check() {
		super.check();
	}
	
	@Override
	public MainControllerMaker make() {
		data();
		out("/easyui/mainController.ftl", data);
		
		return this;
	}
}
