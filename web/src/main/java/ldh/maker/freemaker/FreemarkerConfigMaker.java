package ldh.maker.freemaker;

/**
 * Created by ldh on 2018/2/26.
 */
public class FreemarkerConfigMaker extends FreeMarkerMaker<FreemarkerConfigMaker> {

    protected String basePackage;


    public FreemarkerConfigMaker() {

    }

    public FreemarkerConfigMaker basePackage(String basePackage) {
        this.basePackage = basePackage;
        return this;
    }

    public void data() {
        fileName = "FreemarkerConfig.java";
        data.put("fileName", fileName);
        data.put("basePackage", basePackage);
        check();
    }

    public void check() {
        super.check();
    }

    @Override
    public FreemarkerConfigMaker make() {
        data();
        out(ftl, data);

        return this;
    }
}
