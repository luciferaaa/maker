package ldh.maker.component;

import javafx.scene.control.Tab;
import javafx.scene.control.TreeItem;
import ldh.database.Table;
import ldh.maker.code.CreateCode;
import ldh.maker.code.EasyuiCreateCode;
import ldh.maker.vo.SettingData;
import ldh.maker.vo.TreeNode;

/**
 * Created by ldh on 2017/4/6.
 */
public class EasyuiContentUi extends TableUi {

    public EasyuiContentUi() {
    }

    @Override
    protected Tab createContentTab(String selectTable) {
        Tab tab = new Tab();
        tab.setText(selectTable);
        String db = treeItem.getValue().getData().toString();
        CodeUi codeUi = new EasyuiCodeUi(treeItem, db, selectTable);
        tab.setContent(codeUi);
        return tab;
    }

    @Override
    protected CreateCode buildCreateCode(SettingData data, TreeItem<TreeNode> treeItem, String dbName, Table table) {
        return new EasyuiCreateCode(data, treeItem, dbName, table);
    }
}
