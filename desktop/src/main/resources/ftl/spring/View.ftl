package ${package};

import de.felixroske.jfxsupport.AbstractFxmlView;
import de.felixroske.jfxsupport.FXMLView;

/**
* Created by ldh123 on 2018/6/10.
*/
@FXMLView("${fxml}")
public class ${className} extends AbstractFxmlView {
}
