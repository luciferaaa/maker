package ldh.maker.freemaker;

import ldh.maker.util.FreeMakerUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ldh on 2017/4/23.
 */
public class EnumControllerMaker extends BeanMaker<EnumControllerMaker>  {

    protected List<EnumStatusMaker> enumStatusMakerList = new ArrayList<>();

    public EnumControllerMaker add(EnumStatusMaker enumStatusMaker) {
        enumStatusMakerList.add(enumStatusMaker);
        imports(enumStatusMaker);
        return this;
    }

    @Override
    public EnumControllerMaker make() {
        data();
        out("enumController.ftl", data);

        return this;
    }

    public void data() {
        className = FreeMakerUtil.firstUpper("CommonController");
        fileName = className + ".java";
//        check();
        data.put("enumStatusMakerList", enumStatusMakerList);
        data.put("className", className);
        super.data();
    }
}
